#!/usr/bin/env node

const { execSync } = require("child_process");
const fs = require("fs");
const path = require("path");

const projectName = process.argv[2];

if (!projectName) {
    console.error("Please specify the project name:");
    console.error("  npx create-my-app <project-name>");
    process.exit(1);
}

const projectPath = path.join(process.cwd(), projectName);

if (fs.existsSync(projectPath)) {
    console.error(`The directory ${projectName} already exists in ${process.cwd()}. Please choose another project name.`);
    process.exit(1);
}

try {
    console.log(`Creating a new project in ${projectPath}`);
    execSync(`git clone https://github.com/dotaclover/godance-server-ts.git ${projectPath}`, { stdio: "inherit" });
    //execSync(`rm -rf ${path.join(projectPath, ".git")}`, { stdio: "inherit" });

    deleteGitDirectorySync(projectPath);

    process.chdir(projectPath);
    console.log("Installing dependencies...");
    execSync(`npm install`, { stdio: "inherit" });

    console.log("Project is ready!");
} catch (error) {
    console.error("Error occurred while creating the project:", error);
    process.exit(1);
}


function deleteGitDirectorySync(gitDirPath) {
    try {
        gitDirPath = path.join(gitDirPath || process.cwd(), '.git');
        console.log("gitDirPath", gitDirPath);

        if (fs.existsSync(gitDirPath))
            fs.rmSync(gitDirPath, { recursive: true, force: true });
        console.log('.git directory deleted successfully.');
    } catch (error) {
        console.error('Error deleting .git directory:', error);
    }
}